import { createStore } from 'redux'

const initialState = {
    symbolID: null
}

function symbolReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_SYMBOL':
            return {
                symbolID: action.payload
            }
        default:
            return state
    }
}

export default createStore(symbolReducer)