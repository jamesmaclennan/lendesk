import React from 'react'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import Select from 'react-select'
import { config, fetcher } from '../utils'

function SymbolSelector(props) {
    
    let [options, setOptions] = useState([])
    let [loading, setLoading] = useState(true)

    useEffect(() => {
        const symbolsURL = `https://cloud.iexapis.com/stable/ref-data/symbols?token=${config.apiToken}`
        const getData = async () => {
            let reducedList = []
            let data = await fetcher(symbolsURL).then(response => response.json())
            
            // lightening the load
            for (let i = 0; i < data.length; i++ ) {
				if (i % 50 === 0) {
					reducedList.push({
						value: data[i].symbol,
						label: `${data[i].symbol} - ${data[i].name}`
					})
				}
            }
            setOptions(reducedList)
            setLoading(false)
        }
        
        getData()
        
    }, [])

    function changeHandler(data) {
        props.dispatchSymbol(data.value)
    }

    return (
        <Select 
            placeholder='Select a symbol...'
            className='select'
            classNamePrefix='select'
            onChange={changeHandler}
            options={options}
            isLoading={loading}
        />
    )
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchSymbol: (symbolID) => dispatch({ 
            type: 'SET_SYMBOL',
            payload: symbolID
        })
    }
}

export default connect(null, mapDispatchToProps)(SymbolSelector)