import React from 'react'
import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import Classnames from 'classnames'
import { config, fetcher } from '../utils'

function Symbol(props) {

    let [symbol, setSymbol] = useState()
    let [loading, setLoading] = useState()
    
    useEffect(() => {
        setLoading(true)
        const companyURL = `https://cloud.iexapis.com/stable/stock/${props.symbolID}/company/quote?token=${config.apiToken}`
        const priceURL = `https://cloud.iexapis.com/stable/stock/${props.symbolID}/quote?token=${config.apiToken}`
        if (props.symbolID) {
            const getData = async () => {
                const data = await Promise.all([
                    fetcher(companyURL).then((response) => response.json()),
                    fetcher(priceURL).then((response) => response.json())
                ])
                setSymbol({
                    name: `${data[0].symbol} (${data[0].companyName})`,
                    price: data[1].latestPrice ? `$${data[1].latestPrice.toFixed(2)} USD` : 'No price available...',
                    description: data[0].description || 'No description available...'
                })
                setLoading(false)
            }
            if (props.symbolID) {
                setTimeout(() => {
                    getData()
                }, 250)
            }
        }
    }, [props.symbolID])
    
    return (
        <div 
            className={Classnames('symbol', { 
                'is-loading' : loading
            })}
        >
            { symbol ?
                <dl>
                    <dt>Symbol</dt>
                    <dd>{symbol.name}</dd>
                    <dt>Current Stock Price</dt>
                    <dd>{symbol.price}</dd>
                    <dt>Description</dt>
                    <dd>{symbol.description}</dd>
                </dl>
                : null
            }
        </div>
        
    )
}

function mapStateToProps(state) {
    return {
        symbolID: state.symbolID
    }
}

Symbol.propTypes = {
    symbolID: PropTypes.string
}

export default connect(mapStateToProps)(Symbol)