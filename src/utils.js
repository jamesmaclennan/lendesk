const config = {
    apiToken: 'pk_fde67150d0d140619803e154af0e6ebb'
}

async function fetcher(url) {
    try {
        const response = await fetch(url)
        return await response
    }
    catch(err) {
        throw new Error(err)
    }
}

export { config, fetcher }