import React from 'react'
import { Provider } from 'react-redux'
import SymbolSelector from './components/SymbolSelector'
import Symbol from './components/Symbol'
import store from './store'
import './App.css'

function App() {
	return (
		<Provider store={store}	>
			<div className="App">
				<SymbolSelector />
				<Symbol />
			</div>
		</Provider>
	)
}

export default App
