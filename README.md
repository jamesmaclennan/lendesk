# Lendesk Code Test
### Approach
I spent about 4.5 hours on this code test while trying to keep things simple. I did a couple of new-to-me things in this test. I chose to use React Hooks instead of using the traditional component lifecycle. I also went with async / await for the API calls instead of promises alone.

### Select / Search
I chose to use `react-select` because it provides a select component experience with the ability to integrate search and loading status.

### How-To
This is built using `create-react-app`. `npm install` or `yarn` will install dependencies and `npm start` will start the development environment at `http://localhost:3000`